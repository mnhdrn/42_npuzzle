defmodule Npuzzle.CheckerTest do
  use ExUnit.Case
  # doctest Npuzzle

  alias Npuzzle.Server
  alias Npuzzle.Checker

  setup do
      Server.delete(:test)
  end

  describe "zero?/1" do
    test "zero/1 with unvalid parameter" do
      assert Checker.zero?(:error) == :error
    end

    test "zero/1 with valid board" do
      board = [1, 8, 6, 4, 0, 5, 7, 3]
      Server.update(:origin, board)

      assert Checker.zero?(:ok) == :ok
      Server.delete(:origin)
    end

    test "zero/1 with invalid board" do
      board = [1, 8, 6, 4, 20, 5, 7, 3]
      Server.update(:origin, board)

      assert Checker.zero?(:ok) == :error
      Server.delete(:origin)
    end
  end

  describe "size?/0" do
    test "wrong size" do
      board = [1, 8, 6, 4, 0, 5, 7, 3]
      master = [1, 3, 8, 0, 4, 7, 6, 5]
      Server.update(:origin, board)
      Server.update(:master, master)
      Server.update(:full_size, 9)

      assert Checker.size? == {:error, "Invalid board size"}

      Server.delete(:origin)
      Server.delete(:master)
      Server.delete(:full_size)
    end

    test "valid size" do
      board = [1, 8, 6, 4, 0, 2, 5, 7, 3]
      master = [1, 2, 3, 8, 0, 4, 7, 6, 5]
      Server.update(:origin, board)
      Server.update(:master, master)
      Server.update(:full_size, 9)

      assert Checker.size? == {:ok, "Board size is valid"}

      Server.delete(:origin)
      Server.delete(:master)
      Server.delete(:full_size)
    end
  end

  describe "solvable?/0" do
    test "solvable?/0 with a unsolvable ODD map" do
      board = [0, 2, 1, 8, 7, 6, 5, 4, 3]
      master = [1, 2, 3, 8, 0, 4, 7, 6, 5]
      Server.update(:origin, board)
      Server.update(:master, master)
      Server.update(:size, 3)

      assert Checker.solvable? == false
      Server.delete(:origin)
      Server.delete(:master)
    end

    test "solvable?/0 with a solvable ODD map" do
      board = [4, 3, 0, 2, 5, 6, 1, 8, 7]
      master = [1, 2, 3, 8, 0, 4, 7, 6, 5]
      Server.update(:origin, board)
      Server.update(:master, master)
      Server.update(:size, 3)

      assert Checker.solvable?
      Server.delete(:origin)
      Server.delete(:master)
    end

    test "solvable?/0 with a unsolvable EVEN map" do
      board = [4, 0, 14, 12, 10, 11, 5, 6, 2, 8, 9, 1, 3, 15, 7, 13]
      master = [1, 2, 3, 4, 12, 13, 14, 5, 11, 0, 15, 6, 10, 9, 8, 7]
      Server.update(:origin, board)
      Server.update(:master, master)
      Server.update(:size, 4)

      assert Checker.solvable? == false
      Server.delete(:origin)
      Server.delete(:master)
    end

    test "solvable?/0 with a solvable EVEN map" do
      board = [2, 6, 10, 11, 13, 15, 0, 8, 4, 12, 7, 14, 3, 9, 1, 5]
      master = [1, 2, 3, 4, 12, 13, 14, 5, 11, 0, 15, 6, 10, 9, 8, 7]
      Server.update(:origin, board)
      Server.update(:master, master)
      Server.update(:size, 4)

      assert Checker.solvable? == true
      Server.delete(:origin)
      Server.delete(:master)
    end
  end

end
