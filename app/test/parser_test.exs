defmodule Npuzzle.ParserTest do
  use ExUnit.Case

  alias Npuzzle.Parser

  @err1 "No given file"
  @err2 "Invalid file format or inexistant file"

  describe "parse/1 function" do
    test "parse/1 with no args" do
      assert Parser.parse(1) == {:error, "No given file"}
    end

    test "parse/1 with error" do
      assert Parser.parse({:error, "Bad arguments"}) == {:error, "Bad arguments"}
    end

    test "parse/1 with halt" do
      assert Parser.parse({:halt, "Bad arguments"}) == {:halt, "Bad arguments"}
    end

    test "parse/1 with empty file" do
      assert Parser.parse({:ok, []}) == {:error, @err1}
    end

    test "parse/1 with unexistante file" do
      assert Parser.parse({:ok, ["invalid_token"]}) == {:error, @err2}
    end

    test "format/1 with valid parameter" do
      file = """
      # This puzzle is solvable
      8
      40 25 18 53  6 58 35 16
      46 29 34  7 39 37 47 24
      10  3  8 20 57  2  1 33
      0 23 44 61 31  9 13 41
      15 63 55 49  4 48 45 11
      32 38 52 28 12 51 54 14
      50 43 26 17 42 30 59 22
      5 56 62 60 27 21 19 36
      """

      assert Parser.format({:ok, file}) == :ok
    end

    test "format/1 with comment in a board" do
      file = """
      # This puzzle is solvable
      8
      40 25 18 53  6 58 35 16
      46 29 34  7 39 37 47 24
      10  3  8 20 57  2  1 33
      0 23 44 61 31  9 13 41 # this is a comment
      15 63 55 49  4 48 45 11
      32 38 52 28 12 51 54 14
      50 43 26 17 42 30 59 22
      5 56 62 60 27 21 19 36
      """

      assert Parser.format({:ok, file}) == :ok
    end

  end
end
